CFLAGS=`pkg-config gtk+-2.0 glib-2.0 --cflags`
LIBS=`pkg-config gtk+-2.0 glib-2.0 --libs`
GLIBCFLAGS=`pkg-config glib-2.0 --cflags`
GLIBLIBS=`pkg-config glib-2.0 --libs`

love:

chatcd.c:
chatcd.o: chatcd.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

info.c:
info.o: info.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

parser.c:
parser.o: parser.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

chatsd.c:
chatsd.o: chatsd.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

nchatcd.c:
nchatcd.o: nchatcd.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

nchatsd.c:
nchatsd.o: nchatsd.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

nchatpd.c:
nchatpd.o: nchatpd.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

clinker.c:
clinker.o: clinker.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

pool.c:
pool.o: pool.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

clinker-gtk.c:
clinker-gtk.o: clinker-gtk.c
	gcc -g -Iinc $(CFLAGS) -c src/$^ -o obj/$@

clinker-mysql.c:
clinker-mysql.o: clinker-mysql.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

check.c:
check.o: check.c
	gcc -g -Iinc $(GLIBCFLAGS) -c src/$^ -o obj/$@

chatcd: chatcd.o info.o parser.o
	gcc $(GLIBLIBS) \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` -o $@

chatsd: chatsd.o info.o parser.o clinker-mysql.o
	gcc $(LIBS) -lmysqlclient \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` \
	obj/`echo $^ | cut -d' ' -f4` -o $@

nchatcd: nchatcd.o info.o parser.o
	gcc $(GLIBLIBS) \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` -o $@

nchatsd: nchatsd.o info.o parser.o clinker-mysql.o
	gcc $(GLIBLIBS) -lmysqlclient \
	obj/`echo $^ | \
		cut -d' ' -f1` \
	obj/`echo $^ | \
		cut -d' ' -f2` \
	obj/`echo $^ | \
		cut -d' ' -f3` \
	obj/`echo $^ | \
		cut -d' ' -f4` -o $@

nchatpd: nchatpd.o info.o parser.o check.o clinker-mysql.o
	gcc $(GLIBLIBS) -lmysqlclient -lssl \
	obj/`echo $^ | \
		cut -d' ' -f1` \
	obj/`echo $^ | \
		cut -d' ' -f2` \
	obj/`echo $^ | \
		cut -d' ' -f3` \
	obj/`echo $^ | \
		cut -d' ' -f4` \
	obj/`echo $^ | \
		cut -d' ' -f5` -o $@

clinker: clinker.o info.o parser.o
	gcc $(LIBS) \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` -o $@

pool: pool.o info.o parser.o
	gcc $(GLIBLIBS) \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` -o $@

clinker-gtk: clinker-gtk.o info.o parser.o
	gcc $(LIBS) \
	obj/`echo $^ | cut -d' ' -f1` \
	obj/`echo $^ | cut -d' ' -f2` \
	obj/`echo $^ | cut -d' ' -f3` -o $@

.PHONY: clean
clean:
	rm -f obj/*

.PHONY: all
all: chatcd chatsd nchatcd nchatsd nchatpd clinker-gtk clinker pool

.PHONY: love
love: all

#include "info.h"
#include <openssl/sha.h>

#ifndef CHECK_H
#define CHECK_H

/* hash a string
   return a static pointer */
unsigned char SHA1PASSWD[2*SHA_DIGEST_LENGTH+1];
unsigned char *sha1passwd(const unsigned char *d);

int check_send_file(const struct tmp *buf, struct tmp *resbuf);

int check_passwd(const struct tmp* buf, struct tmp* resbuf);

int check_REG(const struct tmp* buf, struct tmp* resbuf);

int check_ID_passwd(const struct tmp* buf, struct tmp* resbuf);

int checkInfo(const struct tmp *buf, struct tmp *resbuf);

int check_person_info(const struct tmp *buf, struct tmp *resbuf);

#endif

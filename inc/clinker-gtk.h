#include "info.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef CLINKER_GTK_H
#define CLINKER_GTK_H
#define GTK_ENABLE_BROKEN

void login_event(GtkWidget *widget, gpointer data);
int CheckPasswdW(int argc, char *argv[]);
int checkPasswd(int argc, char *argv[]);
void resetPasswd(GtkWidget *widget, gpointer data);

void destroy(GtkWidget *widget, gpointer data);

int ChangeDepartW(int argc, char *argv[]); // 修改弹出部门更改窗口 
int resetDepart(int argc, char *argv[]);
void tesT();

void confirm_register_event(GtkWidget *widget, gpointer data);
int register_event(int argc, char* argv[]);
gboolean combo_event(GtkComboBox *combo1, GtkLabel *labelc);

void loginw(int argc, char *argv[]);

// chat window
void print_data(GtkWidget *widget, char *data);
void on_send();
void clean_send_text();
void show_local_text(const gchar*);
void chatw(int argc, char *argv[]);

// dis find result
GtkWidget *subtree, *item, *subitem;
typedef struct{
    GtkWidget *box;
    GtkWidget *groupname;    
    GtkWidget *button1;
    GtkWidget *tree;
}Tran_para;

int FindPW(int argc, char *argv[]);
void addGroup(GtkWidget *widget, Tran_para *mut);
void addFriend(GtkWidget *widget, gpointer data);
void  disMain(int argc, char *argv[]);
static void cb_selection_changed(GtkWidget *tree);
static void cb_select_child(GtkWidget *root_tree,
        GtkWidget *child, GtkWidget *subtree);
static void cb_unselect_child(GtkWidget *root_tree,
        GtkWidget *child, GtkWidget *subtree);
static void cb_itemsignal(GtkWidget *item, gchar *signame);
void addGroup2(GtkWidget *widget, Tran_para *mut);
void select_row_callback(GtkWidget *clist, gint row, gint column, 
            GdkEventButton *event, gpointer data);
int disFindRes(int argc, char *argv[]);
int disMesg(const struct msg *msgbuf);

#endif

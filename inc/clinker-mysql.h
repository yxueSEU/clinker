#include "info.h"

#ifndef CLINKER_MYSQL_H
#define CLINKER_MYSQL_H

int AddBaseInfo(int argc, char argv[][41], struct person *person);

int AddChatlogs(struct msg*);

int AddFriend(struct proc*);

int Change(int argc, char argv[][6]);

char *getAnswer(int ID);

char *getIP(int ID);

char *getpasswd(int ID);

int hasID(int ID);

int Search(int argc, char *argv[]);

int View(int argc, char *argv[]);

int getState(int ID);

char *getNickname(int ID);

#endif

#include <glib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#ifndef GLOBAL_H
#define GLOBAL_H

/* P0, V0, P1, V1 用于聊天信息
   P2, V2, P3, V3 用于非聊天信息 */
struct sembuf P0, V0, P1, V1, P2, V2, P3, V3;

union semun{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
}arg;

int semid;

int shmid;

GKeyFile *gkfile;
char *gkfile_path;
char *gkfile_value;

char IPbuf[16];
char buf[32];

#endif

#include "global.h"
#include <sys/sem.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <libgen.h>
#include <stdio.h>
#include <pwd.h>

#ifndef INFO_H
#define INFO_H

/* port list */
#define TRANSFER_PORT  6403
#define INFO_PORT      6401


/* checkFlag list */
#define LOGIN              1
#define REGISTER           2
#define CHECK_PASSWD       3
#define RESET_PASSWD       4
#define SEND_FILE          5
#define GET_PERSON_INFO    6

/* chat info struct */
struct msg{
    int sourceID;
    int targetID;
    char msg[BUFSIZ];
};

/* other but chat info struct */
struct proc{
    int flg;
    int argc;
    char argv[10][32];
};

/* person info */
struct person{
    int ID;
    char img[10];
    char nickname[32];
    char department[32];
    char job[32];
    int state;
};

struct tmp{
    struct proc proc;
    struct sockaddr_in addr;
    socklen_t addr_len;
};

int sem_ops();
int sem_init();
int sem_destroy();

int shm_init();
int shm_free();

int writeShm(const struct proc *proc);

int readShm(struct proc *proc);

int sendMsg(const struct msg *msg);

int sendFile(const struct proc *proc);

int recvFile(const struct proc *proc);

char *gethomedir();

int disMsg(const struct msg *msg);

#endif

#include "global.h"
#include <fcntl.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef PARSER_H
#define PARSER_H

int parser_init();
void parser_free();
char *parser_get_value(const char *key);
int parser_set_integer(const char *key, int value);
int parser_set_value(const char *key, const char *value);

#endif

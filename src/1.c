#include "info.h"
#include "parser.h"

int main()
{
    parser_init();

    shmid = atoi(parser_get_value("shmid"));

    struct msg *msgbuf = shmat(shmid, NULL, 0);

    printf("%d %d %s\n", msgbuf->sourceID, msgbuf->targetID, msgbuf->msg);

    parser_free();

    return 0;
}

#include "info.h"
#include "parser.h"

int main()
{
    parser_init();

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    struct msg *msgbuf = shmat(shmid, NULL, 0);


    while(1){
        msgbuf->sourceID = 23;
        msgbuf->targetID = 43;
        scanf("%s", msgbuf->msg);
        semop(semid, &V0, 1);
    }

    shmdt(msgbuf);

    parser_free();

    return 0;
}

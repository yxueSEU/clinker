#include "info.h"
#include "parser.h"

int main()
{
    parser_init();
    shm_init();
    sem_init();
    parser_set_integer("shmid", shmid);
    parser_set_integer("semid", semid);
    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    struct msg *msgbuf = shmat(shmid, NULL, 0);

    while(1){
        semop(semid, &P0, 1);
        printf("%s\n", msgbuf->msg);
    }

    parser_free();
    shm_free();
    sem_destroy();
    return 0;
}

#include "info.h"
#include "parser.h"

int main()
{
    parser_init();

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    struct msg *msgbuf = shmat(shmid, NULL, 0);

    msgbuf->sourceID = 2;
    msgbuf->targetID = 1;

    while(1){
        scanf("%s", msgbuf->msg);
        semop(semid, &V0, 1);
    }

    parser_free();

    return 0;
}

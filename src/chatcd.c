#include "global.h"
#include "info.h"
#include "parser.h"

int main()
{
    int sockfd;
    pid_t pid;
    struct sockaddr_in addr, saddr;
    socklen_t addr_len, saddr_len;
    char localIP[16], serverIP[16];
    struct msg *msgbuf;
    size_t msg_len;

    memset(localIP, 0, sizeof(localIP));
    memset(serverIP, 0, sizeof(serverIP));

    msg_len = sizeof(*msgbuf);
    addr_len = sizeof(addr);
    saddr_len = sizeof(saddr);

    if(parser_init() == -1){
        exit(1);
    }

    strcpy(localIP, parser_get_value("localIP"));
    strcpy(serverIP, parser_get_value("serverIP"));

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

/*    if(sem_init() == -1){
        exit(2);
    }
    if(parser_set_integer("semid", semid) == -1){
        exit(9);
    }

    if(shm_init() == -1){
        exit(3);
    }
    if(parser_set_integer("shmid", shmid) == -1){
        exit(4);
    }
*/
    if((msgbuf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("chatcd.c: shmat");
        exit(5);
    }

    parser_free();

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(localIP);
    addr.sin_port = htons(TRANSFER_PORT);

    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = inet_addr(serverIP);
    saddr.sin_port = htons(TRANSFER_PORT);

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("socket");
        exit(6);
    }

    if((pid = fork()) == -1){
        perror("chatcd.c: fork");
        exit(7);
    }

    if(pid == 0){
        if(bind(sockfd, (struct sockaddr*)&addr, addr_len) == -1){
            perror("chatcd.c: bind");
            exit(8);
        }
        while(1){
            if(recvfrom(sockfd, msgbuf, msg_len, 0,
                        (struct sockaddr*)&saddr, &saddr_len) == -1){
                perror("chatcd.c: recvfrom");
            }
            if(disMsg(msgbuf) == -1){
                fprintf(stderr, "chatcd.c: disMsg");
            }
        }
    }
    else{
        while(1){
            if(semop(semid, &P0, 1) == -1){
                perror("chatcd.c: semop_P0");
                exit(11);
            }
            if(sendto(sockfd, msgbuf, msg_len, 0,
                        (struct sockaddr*)&saddr, saddr_len) == -1){
                perror("chatcd.c: sendto");
           }
        }
    }

    if(close(sockfd) == -1){
        perror("chatcd.c: close");
        exit(24);
    }

    if(shmdt(msgbuf) == -1){
        perror("chatcd.c: shmdt");
        exit(25);
    }

    return 0;
}

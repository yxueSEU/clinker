#include "info.h"
#include "clinker-mysql.h"
#include "parser.h"
    
int main()
{
    int sockfd;
    struct sockaddr_in c1_addr, c2_addr, s_addr;
    socklen_t c1_addr_len, c2_addr_len, s_addr_len;
    char *s_IP, *t_IP, local_IP[16];
    struct msg msgbuf;
    size_t msg_len;
    s_addr_len = sizeof(s_addr);
    c1_addr_len = sizeof(c1_addr);
    c2_addr_len = sizeof(c2_addr);
    msg_len = sizeof(msgbuf);

    memset(local_IP, 0, sizeof(local_IP));

    if(parser_init() == -1)
    {
        exit(1);
    }
    strcpy(local_IP, parser_get_value("localIP"));
    parser_free();
    memset(&s_addr, 0, s_addr_len);
    memset(&c1_addr, 0, c1_addr_len);
    memset(&c2_addr, 0, c2_addr_len);
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = inet_addr(local_IP);
    s_addr.sin_port = htons(TRANSFER_PORT);	
    
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)	
    {
       perror("chatsd.c: socket");
        exit(1);
    }
    if(bind(sockfd, (struct sockaddr *)&s_addr, s_addr_len) == -1)
    {
        perror("chatsd.c: bind");
        exit(1);
    }
    
    while(1)
    {
        if(recvfrom(sockfd, &msgbuf, msg_len, 0,
            (struct sockaddr*)&c1_addr, &c1_addr_len) == -1)
        {
            perror("chatsd.c: recv");
            exit(1);
        }

        t_IP = getIP(msgbuf.targetID);
        AddChatlogs(&msgbuf);
    
        c2_addr.sin_family = AF_INET;
        c2_addr.sin_addr.s_addr = inet_addr(t_IP);
        c2_addr.sin_port = htons(TRANSFER_PORT);
    
        if(sendto(sockfd, &msgbuf, msg_len, 0,
                    (struct sockaddr*)&c2_addr, c2_addr_len) == -1)
        {
            perror("chatsd.c: sendto");
            exit(1);
        }	
    
        memset(&c1_addr, 0, c1_addr_len);
        memset(&c2_addr, 0, c2_addr_len);
    }

    if(close(sockfd) == -1)
    {
        perror("chatsd.c: close");
        exit(1);
    }
    
    return 0;
}

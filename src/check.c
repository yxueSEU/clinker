#include "info.h"
#include "check.h"
#include "clinker-mysql.h"

unsigned char *sha1passwd(const unsigned char *d)
{
    unsigned long n = strlen((char*)d);
    unsigned char md[SHA_DIGEST_LENGTH];

    memset(SHA1PASSWD, 0, sizeof(SHA1PASSWD));
    memset(md, 0, sizeof(md));

    SHA1(d, n, md);

    /* convert unsigned char to hex(char) in order to return */
    int i;
    unsigned char *res_ptr = SHA1PASSWD;
    for(i = 0; i < SHA_DIGEST_LENGTH; i++){
        sprintf(res_ptr, "%02x", md[i]);
        res_ptr += 2;
    }

    return SHA1PASSWD;
}

int checkInfo(const struct tmp *buf, struct tmp *resbuf)
{
    memset(resbuf, 0, sizeof(*resbuf));
    int flag = buf->proc.flg;
    switch (flag)
    {
        case LOGIN:
            return check_ID_passwd(buf, resbuf);
            break ;
        case REGISTER:
            return check_REG(buf, resbuf);
            break ;
        case CHECK_PASSWD:
            return check_passwd(buf, resbuf);
            break ;
        case RESET_PASSWD:
            return check_ID_passwd(buf, resbuf);
            break ;
        case SEND_FILE:
            return check_send_file(buf, resbuf);
            break ;
        case GET_PERSON_INFO:
            return check_person_info(buf, resbuf);
            break ;
        default:
            return -1;
            break ;
    }
}

int check_send_file(const struct tmp *buf, struct tmp *resbuf)
{
    char *IP;

    memcpy(resbuf, buf, sizeof(*resbuf));

    IP = getIP(atoi(resbuf->proc.argv[0]));
    strcpy(resbuf->proc.argv[0], IP);
    IP = getIP(atoi(resbuf->proc.argv[1]));
    strcpy(resbuf->proc.argv[1], IP);

    return 0;
}

int check_ID_passwd(const struct tmp *buf, struct tmp *resbuf)
{
    printf("check.c: check_ID_passwd -----------------------"
            "-----------------\n");
    memcpy(resbuf, buf, sizeof(*resbuf));
    char passwd[41];
    strcpy(passwd, getpasswd(atoi(resbuf->proc.argv[0])));

    if(strcmp(passwd, sha1passwd(resbuf->proc.argv[1])) == 0)
    {
        // send 2, already online
        int state = getState(atoi(resbuf->proc.argv[0]));
        if(state == 1){
            resbuf->proc.argc = 1;
            memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
            strcpy(resbuf->proc.argv[0], "2");
            return -1;
        }
        else{
            char argv[3][6];
            strcpy(argv[0], resbuf->proc.argv[0]);
            strcpy(argv[1], "State");
            strcpy(argv[2], "1");
            // send 3, state change error
            if(Change(3, argv) == -1){
                resbuf->proc.argc = 1;
                memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
                strcpy(resbuf->proc.argv[0], "3");
                return -1;
            }
            // send 0, Success login
            resbuf->proc.argc = 1;
            memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
            strcpy(resbuf->proc.argv[0], "0");
        }
    }
    else
    {
        // send 1, ID:passwd not match
        resbuf->proc.argc = 1;
        memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
        strcpy(resbuf->proc.argv[0], "1");
        fprintf(stderr, "ID donot comply with passwd\n\n");
        return -1;
    }
    return 0;
}

int check_REG(const struct tmp *buf, struct tmp *resbuf)
{
    printf("check.c: check_REG -----------------------------"
            "-----------------\n");
    memcpy(resbuf, buf, sizeof(*resbuf));
    int resHasID;
    resHasID = hasID(atoi(resbuf->proc.argv[0]));
    printf("resHasID: %d\n\n", resHasID);
    
    struct person person;
    char argv[4][41];
    memset(argv, 0, sizeof(argv));
    
    strcpy(argv[0], sha1passwd(resbuf->proc.argv[1]));
    strcpy(argv[1], resbuf->proc.argv[5]);
    strcpy(argv[2], resbuf->proc.argv[6]);
    strcpy(argv[3], inet_ntoa(resbuf->addr.sin_addr));

    person.ID = atoi(resbuf->proc.argv[0]);
    person.state = 0;
    strcpy(person.img, "0-0");
    strcpy(person.nickname, resbuf->proc.argv[2]);
    strcpy(person.job, resbuf->proc.argv[3]);
    strcpy(person.department, resbuf->proc.argv[4]);
    
    if(resHasID == 0)
    {
        // send 2, Error: database insert
	if(AddBaseInfo(4, argv, &person) == -1){
            resbuf->proc.argc = 1;
            memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
            strcpy(resbuf->proc.argv[0], "2");
            return -1;
        }
        // send 0, Success register
        resbuf->proc.argc = 1;
        memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
        strcpy(resbuf->proc.argv[0], "0");
    }
    else
    {
        // send 1, Duplicate ID
        resbuf->proc.argc = 1;
        memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
        strcpy(resbuf->proc.argv[0], "1");
    }

    return 0;
}

int check_passwd(const struct tmp *buf, struct tmp *resbuf)
{
    memcpy(resbuf, buf, sizeof(*resbuf));
    char passwd[41];
    strcpy(passwd, getpasswd(atoi(resbuf->proc.argv[0])));
    
    if(strcmp(passwd, sha1passwd(resbuf->proc.argv[1])) == 0)
    {
        // send 0, ID:passwd match
        resbuf->proc.argc = 1;
        memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
        strcpy(resbuf->proc.argv[0], "0");
    }
    else
    {
        // send 1, ID:passwd not match
        resbuf->proc.argc = 1;
        memset(resbuf->proc.argv, 0, sizeof(resbuf->proc.argv));
        strcpy(resbuf->proc.argv[0], "1");
        return -1;
    }
    return 0;
}

int check_person_info(const struct tmp *buf, struct tmp *resbuf)
{
    int ID;
    
    memcpy(resbuf, buf, sizeof(*resbuf));
    ID = atoi(resbuf->proc.argv[0]);
//    strcpy(resbuf->proc.argv[1], getIMG(ID));
    strcpy(resbuf->proc.argv[2], getNickname(ID));
//    strcpy(resbuf->proc.argv[3], getDepart(ID));
//    strcpy(resbuf->proc.argv[4], getJob(ID));
    sprintf(resbuf->proc.argv[5], "%d", getState(ID));

    return 0;
}

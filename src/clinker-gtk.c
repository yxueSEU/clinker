#include "clinker-gtk.h"
#include "info.h"
#include "parser.h"
#include <gtk/gtk.h>
#include <gtk/gtktree.h>
#include <gtk/gtktreeitem.h>
#include <stdio.h>

GtkWidget *name, *passwd,
          *nickname, *job, 
          *udepartment, *uquestion,
          *answer, *combo1,*combo2,
          *window, *Send_scrolled_win,
          *Rcv_scrolled_win, *Send_textview,
          *Rcv_textview, *SaveButton, *vbox, *vboxR;

GtkTextBuffer *Send_buffer, *Rcv_buffer;

int main(int argc,char *argv[])
{
    loginw(argc, argv);
    return 0;
}

void loginw(int argc, char* argv[])
{
    GtkWidget *window;
    GtkWidget *button;
    GtkWidget *box1;
    GtkWidget *box11;
    GtkWidget *box12;
    GtkWidget *box13;
    GtkWidget *label;

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_signal_connect(GTK_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),NULL);
    box1=gtk_vbox_new(FALSE,0);

    box11=gtk_hbox_new(FALSE,0);
    label=gtk_label_new(" 工号：");
    gtk_box_pack_start(GTK_BOX(box11),label,FALSE,FALSE,5);
    gtk_widget_show(label);

    name=gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(box11),name,FALSE,FALSE,5);
    gtk_widget_show(name);
    gtk_box_pack_start(GTK_BOX(box1),box11,FALSE,FALSE,5);
    gtk_widget_show(box11);

    box12=gtk_hbox_new(FALSE,0);
    label=gtk_label_new(" 密码：");
    gtk_box_pack_start(GTK_BOX(box12),label,FALSE,FALSE,5);
    gtk_widget_show(label);

    passwd=gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(passwd),FALSE);
    gtk_box_pack_start(GTK_BOX(box12),passwd,FALSE,FALSE,5);
    gtk_widget_show(passwd);
    gtk_box_pack_start(GTK_BOX(box1),box12,FALSE,FALSE,5);
    gtk_widget_show(box12);
    
    box13=gtk_hbox_new(FALSE,0);
    button=gtk_button_new_with_label("注册");
    gtk_box_pack_start(GTK_BOX(box13),button,FALSE,FALSE,10);
    gtk_signal_connect(GTK_OBJECT(button),"clicked",G_CALLBACK(register_event),"enter");
    gtk_widget_show(button);
    
    button=gtk_button_new_with_label("登录");
    gtk_box_pack_start(GTK_BOX(box13),button,FALSE,FALSE,10);
    gtk_signal_connect(GTK_OBJECT(button),"clicked",G_CALLBACK(login_event),"enter");
    gtk_widget_show(button);

    button=gtk_button_new_with_label("修改密码");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(CheckPasswdW), NULL);
    gtk_box_pack_start(GTK_BOX(box13),button,FALSE,FALSE,10);
    gtk_widget_show(button);

    button=gtk_button_new_with_label("部门设置");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(ChangeDepartW), NULL);
    gtk_box_pack_start(GTK_BOX(box13),button,FALSE,FALSE,10);
    gtk_widget_show(button);
    
    gtk_box_pack_start(GTK_BOX(box1),box13,FALSE,FALSE,0);
    gtk_widget_show(box13);

    gtk_container_add(GTK_CONTAINER(window),box1);
    gtk_widget_show(box1);
    gtk_widget_show(window);
    gtk_main();
}

void login_event(GtkWidget *widget,gpointer data)
{
    struct proc proc;
    const gchar *uname;
    const gchar *upasswd;
    uname=(gchar *)malloc(sizeof(gchar ));
    upasswd=(gchar *)malloc(sizeof(gchar));
    
    uname=gtk_entry_get_text(GTK_ENTRY(name));
    upasswd=gtk_entry_get_text(GTK_ENTRY(passwd));
    
    proc.flg = LOGIN;
    proc.argc = 2;
    strcpy(proc.argv[0], uname);
    strcpy(proc.argv[1], upasswd);
    
    if(writeShm(&proc) == -1)
    {
        printf("Error: writeShm");
        exit(1);
    }
    if(readShm(&proc) == 0)
    {
        if(strcmp(proc.argv[0], "0") == 0)
        {
            g_print("Success login\n\n");
            disMain(0, NULL);
        }
        if(strcmp(proc.argv[0], "1") == 0)
        {
            g_print("Error: ID:passwd not match\n\n");
        }
        if(strcmp(proc.argv[0], "2") == 0)
        {
            g_print("Error: Already online\n\n");
        }
        if(strcmp(proc.argv[0], "3") == 0)
        {
            g_print("Error: Database state change\n\n");
        }

    }
}

/* 部门设置 */
int ChangeDepartW(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *table;

    GtkWidget *IDname;
    GtkWidget *passwd;

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "部门改换验证");
    gtk_signal_connect(GTK_OBJECT(window), "destroy",
            G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new(3, 4, FALSE);

    label=gtk_label_new("用户名:");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(label);
    IDname=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), IDname, 1, 2, 0, 1,
            GTK_SHRINK,GTK_SHRINK, 5, 5);
    gtk_widget_show(IDname);
    
    label=gtk_label_new("密码:");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(label);
    passwd=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), passwd, 1, 2, 1, 2,
            GTK_SHRINK,GTK_SHRINK, 5, 5);
    gtk_widget_show(passwd);

    button=gtk_button_new_with_label("确定");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(resetDepart), NULL);
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(button);

    button=gtk_button_new_with_label("取消");
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
            G_CALLBACK(destroy), window);
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 2, 3,
            GTK_SHRINK,GTK_SHRINK, 5, 5);
    gtk_widget_show(button);

    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table);

    gtk_widget_show(window);
    gtk_main();
}

void tesT()
{
    g_print("成功更改");
}

int resetDepart(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *table;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *newJob;
    GtkWidget *combo;
    GList *glist=NULL;

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "部门更改");
    gtk_signal_connect(GTK_OBJECT(window), "destroy",
            G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new(3, 4, FALSE);

    label=gtk_label_new("部门：");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(label);

    combo=gtk_combo_new();
    glist=g_list_append(glist, "财务部");
    glist=g_list_append(glist, "技术部");
    glist=g_list_append(glist, "销售部");
    gtk_combo_set_popdown_strings(GTK_COMBO(combo), glist);
    gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(combo);

    label=gtk_label_new("职务：");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(label);

    newJob=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), newJob, 1, 2, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_widget_show(newJob);

    button=gtk_button_new_with_label("确定");
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3, 
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(tesT), NULL);
    gtk_widget_show(button);
    
    button=gtk_button_new_with_label("取消");
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 2, 3, 
            GTK_SHRINK, GTK_SHRINK, 10, 5);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(destroy), window);
    gtk_widget_show(button);

    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table);
    gtk_widget_show(window);

    gtk_main();
}

int CheckPasswdW(int argc, char *argv[])
{
    GtkWidget *window;

    GtkWidget *IDname;
    GtkWidget *table;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *passwdQ;
    GtkWidget *combo;
    GList *glist=NULL;

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_signal_connect(GTK_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new(3, 5,FALSE);

    label=gtk_label_new("用户名：");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(label);
    IDname=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), IDname, 1, 2, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(IDname);

    label=gtk_label_new("您的密码问题：");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(label);

    combo=gtk_combo_new();
    glist=g_list_append(glist, "Your birthday？");
    glist=g_list_append(glist, "Your father's name？");
    glist=g_list_append(glist, "Your mother's name？");
    gtk_combo_set_popdown_strings(GTK_COMBO(combo), glist);
    gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 1, 2,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(combo);

    label=gtk_label_new("答案：");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(label);
    passwdQ=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), passwdQ, 1, 2, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(passwdQ);
    
    button=gtk_button_new_with_label("确定");
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
            G_CALLBACK(checkPasswd), window);
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 3, 4,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(button);

    button=gtk_button_new_with_label("取消");
    gtk_signal_connect(GTK_OBJECT(button), "clicked", G_CALLBACK(destroy), window);
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 3, 4,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(button);

    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table);
    gtk_widget_show(window);
    gtk_main();
    return 0;
}


GtkWidget *newPasswd;
GtkWidget *newPasswdCon;
int checkPasswd(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *label;
    GtkWidget *button;
    GtkWidget *table;
    
    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "设定新密码");
    gtk_signal_connect(GTK_OBJECT(window), "destroy",
            G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new(3, 4, FALSE);

    label=gtk_label_new("新密码");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(label);

    newPasswd=gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(newPasswd), FALSE);
    gtk_table_attach(GTK_TABLE(table), newPasswd, 1, 2, 0, 1,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(newPasswd);

    label=gtk_label_new("确认新密码");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(label);

    newPasswdCon=gtk_entry_new();
    gtk_entry_set_visibility(GTK_ENTRY(newPasswdCon), FALSE);
    gtk_table_attach(GTK_TABLE(table), newPasswdCon, 1, 2, 1, 2,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(newPasswdCon);

    button=gtk_button_new_with_label("确定");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(resetPasswd), window);
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(button);

    button=gtk_button_new_with_label("取消");
    gtk_signal_connect(GTK_OBJECT(button), "clicked", G_CALLBACK(destroy), window);
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 0, 0);
    gtk_widget_show(button);
    
    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table);

    gtk_widget_show(window);
    gtk_main();
}

void resetPasswd(GtkWidget *widget, gpointer data)
{
    const gchar *newp1;
    const gchar *newp2;

    newp1=(gchar *)malloc(sizeof(gchar));
    newp2=(gchar *)malloc(sizeof(gchar));

    newp1=gtk_entry_get_text(GTK_ENTRY(newPasswd));
    newp2=gtk_entry_get_text(GTK_ENTRY(newPasswdCon));
    g_print("1: %s\n", newp1);
    g_print("2: %s\n", newp2);
    gtk_widget_destroy(GTK_WIDGET(data));
}

gboolean combo_event(GtkComboBox *combo1,GtkLabel *labelc)
{
    gchar* active = gtk_combo_box_get_active_text(combo1);
    gtk_label_set_text(labelc,active);
}

int register_event(int argc,char* argv[])
{
    GtkWidget* window,*table,*label, *button;
    GtkWidget* labelc,*labelcc;

    gtk_init(&argc,&argv);
    
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    
    gtk_window_set_title(GTK_WINDOW(window),"注册窗口");
    gtk_signal_connect(GTK_OBJECT(window), "destroy",
        G_CALLBACK(gtk_main_quit),NULL);

//create new table!!!
    table = gtk_table_new(9,3,FALSE);
//create new label and entry
    label = gtk_label_new("工号:");
    gtk_table_attach(
            GTK_TABLE(table),
            label, 0,1,0,1,
            GTK_FILL,GTK_FILL,0,0);
    name = gtk_entry_new();
    //table attach
    gtk_table_attach(
                GTK_TABLE(table),
                name, 1,2,0,1, 
                GTK_FILL,GTK_FILL,0,0);

    //end gonghao
  /* PackLabelAndEntry(GtkWidget*,"Number:","");
   PackLabelAndEntry(GtkWidget*,"Code:",""); 
   PackLabelAndEntry(GtkWidget*,"Nickname:",""); 
   PackLabelAndEntry(GtkWidget*,"Position:",""); 
    */
    //caidan here*******************

   
  // PackLabelAndEntry(GtkWidget*,"Answer:",""); 
 label = gtk_label_new("密码:");  
 gtk_table_attach(GTK_TABLE(table),
                label, 0,1,1,2,
     GTK_FILL,GTK_FILL,0,0);
 passwd = gtk_entry_new();
 gtk_entry_set_visibility(GTK_ENTRY(passwd),FALSE);
 gtk_table_attach(
           GTK_TABLE(table),
           passwd, 1,2,1,2, 
           GTK_FILL,GTK_FILL,0,0);
//***********
 label = gtk_label_new("昵称");
 gtk_table_attach(GTK_TABLE(table),
               label, 0,1,2,3,
               GTK_FILL,GTK_FILL,0,0);
nickname = gtk_entry_new();
gtk_table_attach(
                GTK_TABLE(table),nickname, 1,2,2,3,
                GTK_FILL,GTK_FILL,0,0);
   //*************
label = gtk_label_new("职务:");
gtk_table_attach(
                GTK_TABLE(table),
                label, 0,1,3,4,
                GTK_FILL,GTK_FILL,0,0);
job = gtk_entry_new();
gtk_table_attach(
                GTK_TABLE(table),job, 1,2,3,4,
                GTK_FILL,GTK_FILL,0,0);
//
 label = gtk_label_new("部门:");
 gtk_table_attach(GTK_TABLE(table),
                 label, 0,1,4,5,
                GTK_FILL,GTK_FILL,0,0);
 //*************ComboBox*************88
 combo1 = gtk_combo_box_new_text();
 combo2 = gtk_combo_box_new_text();
 gtk_combo_box_append_text(GTK_COMBO_BOX(combo1),"财务部");
 gtk_combo_box_append_text(GTK_COMBO_BOX(combo1),"技术部");
 gtk_combo_box_append_text(GTK_COMBO_BOX(combo1),"销售部");
 gtk_combo_box_set_active(GTK_COMBO_BOX(combo1),0);
 labelc = gtk_label_new("label new1");
 g_signal_connect(GTK_OBJECT(combo1),"changed",G_CALLBACK(combo_event),
         labelc);
 gtk_table_attach(GTK_TABLE(table),combo1,1,2,4,5,GTK_FILL,GTK_FILL,0,0);
   
//**************combo2**************
   label = gtk_label_new("密保问题:");
   gtk_table_attach(GTK_TABLE(table),
                 label, 0,1,5,6,GTK_FILL,GTK_FILL,0,0);
    
   gtk_combo_box_append_text(GTK_COMBO_BOX(combo2),"Your birthday?");
   gtk_combo_box_append_text(GTK_COMBO_BOX(combo2),"Your father's name?");
   gtk_combo_box_append_text(GTK_COMBO_BOX(combo2),"Your mother's name?");


   gtk_combo_box_set_active(GTK_COMBO_BOX(combo2),0);
   labelcc = gtk_label_new("label new2");
   g_signal_connect(GTK_OBJECT(combo2),"changed",G_CALLBACK(combo_event),labelcc );
   gtk_table_attach(GTK_TABLE(table),combo2,1,2,5,6,GTK_FILL,GTK_FILL,0,0);


 //******************
   label = gtk_label_new("答案:");
   gtk_table_attach(GTK_TABLE(table),label, 0,1,6,7,
        GTK_FILL,GTK_FILL,0,0);
   answer = gtk_entry_new();
   gtk_table_attach(GTK_TABLE(table),answer, 1,2,6,7,
                 GTK_FILL,GTK_FILL,0,0);

 //***********************
   button = gtk_button_new_with_label("确认");
   gtk_table_attach(
            GTK_TABLE(table),button,
            0,1,8,9,
            GTK_SHRINK,GTK_SHRINK,0,0);   
   gtk_signal_connect(
            GTK_OBJECT(button),"clicked",
            GTK_SIGNAL_FUNC(confirm_register_event/* 接口，替代之*/),
           "window");
    
    button = gtk_button_new_with_label("取消"); 
    gtk_table_attach(
            GTK_TABLE(table),button,
            1,2,8,9,
            GTK_SHRINK,GTK_SHRINK,0,0);
    gtk_widget_show(button);

    gtk_signal_connect(
            GTK_OBJECT(button),"clicked",
            GTK_SIGNAL_FUNC(gtk_main_quit/*同上*/),
            NULL);


    gtk_container_add(GTK_CONTAINER(window),table);
    gtk_widget_show_all(window);
    gtk_main();
    return FALSE;
}//end main

void confirm_register_event(GtkWidget *widget, gpointer data)
{
    GtkWidget* window;
    struct proc proc;
    const gchar *uname;
    const gchar *upasswd;
    const gchar *unickname;
    const gchar *ujob;
    const gchar *udepartment;
    const gchar *uquestion;
    const gchar *uanswer;

 //   gtk_widget_destroy(GTK_WIDGET(data));

    uname = (gchar *)malloc(sizeof(gchar ));
    upasswd = (gchar *)malloc(sizeof(gchar ));
    unickname = (gchar *)malloc(sizeof(gchar ));
    ujob = (gchar *)malloc(sizeof(gchar ));
//    department = (gchar *)malloc(sizeof(gchar ));
    udepartment = gtk_combo_box_get_active_text(GTK_COMBO_BOX(combo1));  
//question =(gchar *)malloc(sizeof(gchar ));
    uquestion = gtk_combo_box_get_active_text(GTK_COMBO_BOX(combo2));
    
    uanswer = (gchar *)malloc(sizeof(gchar ));
    uname = gtk_entry_get_text(GTK_ENTRY(name));
    upasswd = gtk_entry_get_text(GTK_ENTRY(passwd));
    unickname = gtk_entry_get_text(GTK_ENTRY(nickname));
 //   udepartment = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO_BOX(combo1)->entry));
    ujob = gtk_entry_get_text(GTK_ENTRY(job));
    uanswer = gtk_entry_get_text(GTK_ENTRY(answer));
    proc.flg = REGISTER;
    proc.argc = 7;
    strcpy(proc.argv[0], uname);
    strcpy(proc.argv[1], upasswd);
    strcpy(proc.argv[2], unickname);
    strcpy(proc.argv[3], ujob);
    strcpy(proc.argv[4], udepartment);
    strcpy(proc.argv[5], uquestion);
    strcpy(proc.argv[6], uanswer);

    g_print("%s\n", uname);

    if(writeShm(&proc) == -1)
    {
        exit(1);
    }
    if(readShm(&proc) == 0)
    {
        printf("argv[0]: %s\n", proc.argv[0]);
        if(strcmp(proc.argv[0], "0") == 0)
        {
            g_print("Success register\n\n");
            loginw(0, 0);
        }
        if(strcmp(proc.argv[0], "1") == 0)
        {
            g_print("Error: duplicate ID\n\n");
        }
        if(strcmp(proc.argv[0], "2") == 0)
        {
            g_print("Error: Database insert\n\n");
        }
    }
}

/*
void on_send(GtkButton *SaveButton, GtkWidget *Send_textview)
{

GtkTextBuffer *S_buffer,*R_buffer;
GtkTextIter start,end;

gchar *S_text,*R_text;

S_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(Send_textview));
gtk_text_buffer_get_start_iter(Send_buffer,&start);
gtk_text_buffer_get_end_iter(Send_buffer,&end);
S_text = gtk_text_buffer_get_text(Send_buffer,&start,&end,TRUE);

g_print("%s\n",S_text);
//g_print(S_text);

R_buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(Rcv_textview));

R_text = S_text;

gtk_text_buffer_set_text(R_buffer,R_text,-1);

gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(S_buffer),&start,&end);
gtk_text_buffer_delete(GTK_TEXT_BUFFER(S_buffer),&start,&end);

}
*/

void chatw(int argc, char *argv[])
{
    GtkWidget *paned,*toolbar;


gtk_init(&argc,&argv);
/*---------------------------------------------------------*/

window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

g_signal_connect(G_OBJECT(window),"delete_event",
        G_CALLBACK(gtk_main_quit),NULL);
gtk_window_set_title(GTK_WINDOW(window),"聊天窗口");

gtk_container_set_border_width(GTK_CONTAINER(window),10);

gtk_widget_set_size_request(window,800,500);

gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_CENTER);

paned=gtk_hpaned_new();
vbox = gtk_vbox_new(FALSE,0);
vboxR = gtk_vbox_new(FALSE,0);
//create toolbar
toolbar = gtk_toolbar_new();

//  imag = gtk_imag_new_from_file("fileName");

gtk_toolbar_append_item(GTK_TOOLBAR(toolbar),"聊天记录","点击查看聊天记录",NULL,NULL,(GtkSignalFunc)print_data,"toolbarButton callback");

gtk_toolbar_append_item(GTK_TOOLBAR(toolbar),"上传","上传文件",NULL,NULL,(GtkSignalFunc)print_data,"toolbarButton callback");
/*---------------------Send_text view---------------*/

Send_textview = gtk_text_view_new();

gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(Send_textview),GTK_WRAP_WORD);

gtk_text_view_set_justification(GTK_TEXT_VIEW(Send_textview),GTK_JUSTIFY_LEFT);

gtk_text_view_set_editable(GTK_TEXT_VIEW(Send_textview),TRUE);

gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(Send_textview),TRUE);

gtk_text_view_set_pixels_above_lines(GTK_TEXT_VIEW(Send_textview),5);

gtk_text_view_set_pixels_below_lines(GTK_TEXT_VIEW(Send_textview),5);

gtk_text_view_set_pixels_inside_wrap(GTK_TEXT_VIEW(Send_textview),5);

gtk_text_view_set_left_margin(GTK_TEXT_VIEW(Send_textview),10);

gtk_text_view_set_right_margin(GTK_TEXT_VIEW(Send_textview),10);

Send_buffer =  gtk_text_view_get_buffer(GTK_TEXT_VIEW(Send_textview));


Rcv_textview = gtk_text_view_new();

gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(Rcv_textview),GTK_WRAP_WORD);

gtk_text_view_set_justification(GTK_TEXT_VIEW(Rcv_textview),GTK_JUSTIFY_LEFT);

gtk_text_view_set_editable(GTK_TEXT_VIEW(Rcv_textview),FALSE);

gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(Rcv_textview),TRUE);

gtk_text_view_set_pixels_above_lines(GTK_TEXT_VIEW(Rcv_textview),5);

gtk_text_view_set_pixels_below_lines(GTK_TEXT_VIEW(Rcv_textview),5);

gtk_text_view_set_pixels_inside_wrap(GTK_TEXT_VIEW(Rcv_textview),5);

gtk_text_view_set_left_margin(GTK_TEXT_VIEW(Rcv_textview),10);

gtk_text_view_set_right_margin(GTK_TEXT_VIEW(Rcv_textview),10);
Rcv_buffer =  gtk_text_view_get_buffer(GTK_TEXT_VIEW(Rcv_textview));

/*------------------------------Send scrolled window--------------------*/
Send_scrolled_win = gtk_scrolled_window_new(NULL,NULL);

gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Send_scrolled_win),GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS);


Rcv_scrolled_win = gtk_scrolled_window_new(NULL,NULL);

gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(Rcv_scrolled_win),GTK_POLICY_AUTOMATIC,GTK_POLICY_ALWAYS);


vbox = gtk_vbox_new(FALSE,0);


SaveButton = gtk_button_new_with_label("发送");

g_signal_connect(G_OBJECT(SaveButton),"clicked",G_CALLBACK(on_send),Send_buffer);

//here

gtk_container_add(GTK_CONTAINER(Send_scrolled_win),Send_textview);

gtk_container_add(GTK_CONTAINER(Rcv_scrolled_win),Rcv_textview);
gtk_container_add(GTK_CONTAINER(vbox),Rcv_scrolled_win);
gtk_box_pack_start(GTK_BOX(vbox),toolbar,FALSE,FALSE,5);

gtk_container_add(GTK_CONTAINER(vbox),Send_scrolled_win);
gtk_box_pack_start(GTK_BOX(vbox),SaveButton,FALSE,FALSE,5);

GtkWidget* frame1,*frame2,*label1,*label2;
frame1 = gtk_frame_new("my information");
frame2 = gtk_frame_new("the other's information");
gtk_box_pack_start (GTK_BOX(vboxR),frame1,TRUE,TRUE,10);
gtk_box_pack_start(GTK_BOX(vboxR),frame2,TRUE,TRUE,10);
//gtk_frame_set_label_align(GTK_FRAME (frame1),0.5,0.5);

gtk_paned_pack1(GTK_PANED(paned),vbox,FALSE,FALSE);

gtk_paned_pack2(GTK_PANED(paned),vboxR,FALSE,FALSE);

gtk_container_add(GTK_CONTAINER(window),paned);
gtk_widget_show_all(window);
gtk_main();
}

void print_data(GtkWidget *widget,char* data)
{ 
    g_print("%s\n", data);
}

void on_send()
{
    GtkTextIter start,end;
    char *text;
    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(Send_buffer),&start,&end);
    text = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(Send_buffer),&start,&end,FALSE);
   // if(strcmp(text,"")!=0)
   struct msg msgbuf;
    msgbuf.sourceID = 1;
    msgbuf.targetID = 1;
    strcpy(msgbuf.msg, text);
    sendMsg(&msgbuf);
       
        clean_send_text();
        show_local_text(text);
}

void clean_send_text()
{
    GtkTextIter start,end;
    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(Send_buffer),&start,&end);
    gtk_text_buffer_delete(GTK_TEXT_BUFFER(Send_buffer),&start,&end);
}

void show_local_text(const gchar *text)
{
    GtkTextIter start,end;
    gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(Rcv_buffer),&start,&end);
    gtk_text_buffer_insert(GTK_TEXT_BUFFER(Rcv_buffer),&end,"我说:\n",8);
    gtk_text_buffer_insert(GTK_TEXT_BUFFER(Rcv_buffer),&end,text,strlen(text));
    gtk_text_buffer_insert(GTK_TEXT_BUFFER(Rcv_buffer),&end,"\n",1);
}

int disFindRes(int argc, char *argv[])
{
    GtkWidget *window, *clist, *button, *table;
    gchar *TEXT;
    //gchar *people[4];

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "查找结果");
    gtk_widget_set_size_request(window, 250, 100);
    gtk_signal_connect(GTK_OBJECT(window), "destroy", 
            G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new(3, 3, FALSE);

    clist=gtk_clist_new(4);
    gtk_clist_set_column_title(GTK_CLIST(clist), 0, "   工号   ");
    gtk_clist_set_column_title(GTK_CLIST(clist), 1, "   昵称   ");
    gtk_clist_set_column_title(GTK_CLIST(clist), 2, "   部门   ");
    gtk_clist_set_column_title(GTK_CLIST(clist), 3, "   职务   ");

    gtk_clist_column_titles_show(GTK_CLIST(clist));
    gtk_table_attach(GTK_TABLE(table), clist, 0, 2, 0, 1, 
            GTK_EXPAND, GTK_EXPAND, 0 , 0);
    gtk_widget_show(clist);
    gchar *text[]={"1528","wocao","xiaoshou","chifan"};
/*int i;
for(i = 0; i < 4; i++){g_print("%s\n", text[i]);}

    gtk_clist_prepend(GTK_CLIST(clist), data);*/
    gtk_clist_prepend(GTK_CLIST(clist), text);

    button=gtk_button_new_with_label(" 添加 ");
    gtk_signal_connect(GTK_OBJECT(button),  "clicked", 
            G_CALLBACK(addFriend), text[0]);
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10 , 10);
    gtk_widget_set_sensitive(button, FALSE); 
    gtk_widget_show(button);
    gtk_signal_connect(GTK_OBJECT(clist), "select_row", 
            G_CALLBACK(select_row_callback), button);

    button=gtk_button_new_with_label("取消");
    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
            G_CALLBACK(destroy), window);
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10 , 10);
    gtk_widget_show(button);

    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table); 
    gtk_widget_show(window);
    gtk_main();
}

void select_row_callback(GtkWidget *clist, gint row, gint column, 
            GdkEventButton *event, gpointer data)
{
    gtk_widget_set_sensitive(data, TRUE);
    gchar *teXt=NULL;
    gtk_clist_get_text(GTK_CLIST(clist), row, column, &teXt);
    g_print("%s\n", teXt);
}

void destroy(GtkWidget *widget, gpointer data)
{
    gtk_widget_destroy(GTK_WIDGET(data));
}

  
void addGroup2(GtkWidget *widget, Tran_para *mut)
{   
    const gchar *ugroupname;
    ugroupname=(gchar *)malloc(sizeof(gchar));
    ugroupname=gtk_entry_get_text(GTK_ENTRY(((Tran_para *)mut)->groupname));

    g_print("%s\n", ugroupname);

    item = gtk_tree_item_new_with_label (ugroupname);
        /* Connect all GtkItem:: and GtkTreeItem:: signals */
    g_signal_connect (G_OBJECT (item), "select",
            G_CALLBACK (cb_itemsignal), "select");
    g_signal_connect (G_OBJECT (item), "deselect",
            G_CALLBACK (cb_itemsignal), "deselect");
        g_signal_connect (G_OBJECT (item), "toggle",
                G_CALLBACK (cb_itemsignal), "toggle");
        g_signal_connect (G_OBJECT (item), "expand",
                G_CALLBACK (cb_itemsignal), "expand");
        g_signal_connect (G_OBJECT (item), "collapse",
                G_CALLBACK (cb_itemsignal), "collapse");
        /* Add it to the parent tree */
    gtk_tree_append (GTK_TREE(((Tran_para *)mut)->tree), item); 
        /* Show it - this can be done at any time */
    gtk_widget_show (item);
        /*Create this item's subtree */
    subtree = gtk_tree_new ();
            
    g_signal_connect (G_OBJECT (subtree), "select_child",
          G_CALLBACK (cb_select_child), subtree);
    g_signal_connect (G_OBJECT (subtree), "unselect_child",
          G_CALLBACK (cb_unselect_child), subtree);
 
    gtk_tree_set_selection_mode (GTK_TREE (subtree),
          GTK_SELECTION_SINGLE);

    gtk_tree_set_view_mode (GTK_TREE (subtree), GTK_TREE_VIEW_ITEM);
         
    gtk_tree_item_set_subtree (GTK_TREE_ITEM (item), subtree);
    gtk_widget_hide(((Tran_para *)mut)->box);
}


static void cb_itemsignal( GtkWidget *item, gchar *signame )
{
    gchar *name;
    GtkLabel *label;
    /* It's a Bin, so it has one child, which we know to be a
       label, so get that */
    label = GTK_LABEL (GTK_BIN (item)->child);
    /* Get the text of the label */
    gtk_label_get (label, &name);
    
    /*g_print ("%s called for item %s->%p, level %d\n", signame, name,
            item, GTK_TREE (item->parent)->level);*/
}
/* Note that this is never called */
static void cb_unselect_child( GtkWidget *root_tree,
        GtkWidget *child, GtkWidget *subtree )
{
    g_print ("unselect_child called for root tree %p, subtree %p, child %p\n", root_tree, subtree, child);
}
/* Note that this is called every time the user clicks on an item,
   whether it is already selected or not. */
static void cb_select_child (GtkWidget *root_tree, GtkWidget *child,
        GtkWidget *subtree)
{
    /*g_print ("select_child called for root tree %p, subtree %p, child %p\n",
            root_tree, subtree, child);双击反应*/
    gtk_main_quit();
}

static void cb_selection_changed( GtkWidget *tree )
{
    GList *i;
    g_print ("selection_change called for tree %p\n", tree);
    g_print ("selected objects are:\n");
    i = GTK_TREE_SELECTION_OLD (tree);
    while (i) {
        gchar *name;
        GtkLabel *label;
        GtkWidget *item;
        /* Get a GtkWidget pointer from the list node */
        item = GTK_WIDGET (i->data);
        label = GTK_LABEL (GTK_BIN (item)->child);
        gtk_label_get (label, &name);
        g_print ("\t%s on level %d\n", name, GTK_TREE
                (item->parent)->level);
        i = i->next;
    }
}

void disMain(int argc, char *argv[])
{
    GtkWidget *window, *scrolled_win, *label,
              *table, *button, *image;
    Tran_para *mut_para;
    Tran_para mupara;
    mut_para = &mupara;
       /*GtkWidget *box;
       GtkWidget *groupname;    
       GtkWidget *button1;*/
    static gchar *itemnames[] ={"技术部", "财务部", "销售部"};
    static gchar *itemnamesss[] ={"yyyyy", "uuuuu", "iiiii", "ooooo", "ppppp"};

    gint i;
        
    gtk_init (&argc, &argv);
    /* a generic toplevel window */
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    g_signal_connect (G_OBJECT (window), "delete_event",
            G_CALLBACK (gtk_main_quit), NULL);
    table=gtk_table_new(3, 6,FALSE);

    label=gtk_label_new("IDname");
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 0, 1, 
            GTK_FILL, GTK_FILL, 5, 5);
    gtk_widget_show(label);

    label=gtk_label_new("nickname");
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 1, 2, 
            GTK_FILL, GTK_FILL, 5, 5);
    gtk_widget_show(label);

    mut_para->tree = gtk_tree_new ();

    image=gtk_image_new_from_file("touxiang.bmp");
    gtk_table_attach(GTK_TABLE(table), image, 0, 1, 1, 2, 
            GTK_FILL, GTK_FILL, 5, 5);
    gtk_widget_show(image);

    gtk_container_set_border_width (GTK_CONTAINER (window), 5);
    /* A generic scrolled window */
    scrolled_win = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
            GTK_POLICY_AUTOMATIC,
            GTK_POLICY_AUTOMATIC);
    gtk_widget_set_size_request (scrolled_win, 200, 200);

    gtk_table_attach (GTK_TABLE(table), scrolled_win, 0, 2, 2, 3,
            GTK_FILL, GTK_FILL, 5, 5);
    gtk_widget_show(table);
    gtk_container_add (GTK_CONTAINER (window), table);
    gtk_widget_show (scrolled_win);
    /* Create the root tree */
        /*g_print ("root tree is %p\n", tree);
     connect all GtkTree:: signals */
    g_signal_connect (G_OBJECT (mut_para->tree), "select_child",
            G_CALLBACK (cb_select_child), mut_para->tree);
    g_signal_connect (G_OBJECT (mut_para->tree), "unselect_child",
            G_CALLBACK (cb_unselect_child), mut_para->tree);
    g_signal_connect (G_OBJECT(mut_para->tree), "selection_changed",
            G_CALLBACK(cb_selection_changed), mut_para->tree);
    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_win),
            mut_para->tree);
    /* Set the selection mode */
    gtk_tree_set_selection_mode (GTK_TREE (mut_para->tree),
            GTK_SELECTION_MULTIPLE);
    /* Show it */
    gtk_widget_show (mut_para->tree);
    for (i = 0; i < 3; i++){
        
        gint j;
        /* Create a tree item */
        item = gtk_tree_item_new_with_label (itemnames[i]);
        /* Connect all GtkItem:: and GtkTreeItem:: signals */
        g_signal_connect (G_OBJECT (item), "select",
                G_CALLBACK (cb_itemsignal), "select");
        g_signal_connect (G_OBJECT (item), "deselect",
                G_CALLBACK (cb_itemsignal), "deselect");
        g_signal_connect (G_OBJECT (item), "toggle",
                G_CALLBACK (cb_itemsignal), "toggle");
        g_signal_connect (G_OBJECT (item), "expand",
                G_CALLBACK (cb_itemsignal), "expand");
        g_signal_connect (G_OBJECT (item), "collapse",
                G_CALLBACK (cb_itemsignal), "collapse");
        /* Add it to the parent tree */
        gtk_tree_append (GTK_TREE (mut_para->tree), item);
        /* Show it - this can be done at any time */
        gtk_widget_show (item);
        /* Create this item's subtree */
        subtree = gtk_tree_new ();
        /*g_print ("-> item %s->%p, subtree %p\n", itemnames[i], item,
                subtree);*/
        
        g_signal_connect (G_OBJECT (subtree), "select_child",
                G_CALLBACK (cb_select_child), subtree);
        g_signal_connect (G_OBJECT (subtree), "unselect_child",
                G_CALLBACK (cb_unselect_child), subtree);
        /* This has absolutely no effect, because it is completely ignored
           in subtrees */
        gtk_tree_set_selection_mode (GTK_TREE (subtree),
                GTK_SELECTION_SINGLE);

            gtk_tree_set_view_mode (GTK_TREE (subtree), GTK_TREE_VIEW_ITEM);
         
            gtk_tree_item_set_subtree (GTK_TREE_ITEM (item), subtree);

        for (j = 0; j < 5; j++){
            
            /* Create a subtree item, in much the same way */
            subitem = gtk_tree_item_new_with_label (itemnamesss[j]);
            /* Connect all GtkItem:: and GtkTreeItem:: signals */
            g_signal_connect (G_OBJECT (subitem), "select",
                    G_CALLBACK (cb_itemsignal), "select");
            g_signal_connect (G_OBJECT (subitem), "deselect",
                    G_CALLBACK (cb_itemsignal), "deselect");
            g_signal_connect (G_OBJECT (subitem), "toggle",
                    G_CALLBACK (cb_itemsignal), "toggle");
            g_signal_connect (G_OBJECT (subitem), "expand",
                    G_CALLBACK (cb_itemsignal), "expand");
            g_signal_connect (G_OBJECT (subitem), "collapse",
                    G_CALLBACK (cb_itemsignal), "collapse");
            /*g_print ("-> -> item %s->%p\n", itemnamesss[j], subitem);*/
            
            gtk_tree_append (GTK_TREE (subtree), subitem);
            
            gtk_widget_show (subitem);
        }
    }
    button=gtk_button_new_with_label("  查找  ");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(FindPW), NULL);
    gtk_table_attach(GTK_TABLE(table), button, 1, 2, 3, 4, 
            GTK_SHRINK, GTK_SHRINK, 5, 5);
    gtk_widget_show(button);

    mut_para->box=gtk_hbox_new(FALSE, 10);

    mut_para->groupname=gtk_entry_new();
    gtk_box_pack_start(GTK_BOX(mut_para->box), mut_para->groupname, FALSE, FALSE, 5);
    gtk_widget_show(mut_para->groupname);

    mut_para->button1=gtk_button_new_with_label("  确 定  ");

    gtk_box_pack_start(GTK_BOX(mut_para->box), mut_para->button1, FALSE, FALSE, 5);
    gtk_widget_show(mut_para->button1);
    gtk_table_attach(GTK_TABLE(table), mut_para->box, 0, 2, 4, 5, 
            GTK_SHRINK, GTK_SHRINK, 5, 5);
    gtk_signal_connect(GTK_OBJECT(mut_para->button1), "clicked", 
            G_CALLBACK(addGroup2), mut_para);

    button=gtk_button_new_with_label("添加分组");
    
    //Tran_para *mut_para;

    gtk_signal_connect(GTK_OBJECT(button), "clicked", 
	    G_CALLBACK(addGroup), mut_para);
    gtk_table_attach(GTK_TABLE(table), button, 0, 1, 3, 4, 
            GTK_SHRINK, GTK_SHRINK, 5, 5);
    gtk_widget_show(button);
    
    gtk_widget_show (window);
    gtk_main();
}

void addFriend(GtkWidget *widget, gpointer data)
{
    g_print("FFFFFFFRRRRRDDD");
    subitem = gtk_tree_item_new_with_label (data);

    g_signal_connect (G_OBJECT (subitem), "select",
                    G_CALLBACK (cb_itemsignal), "select");
    g_signal_connect (G_OBJECT (subitem), "deselect",
                    G_CALLBACK (cb_itemsignal), "deselect");
    g_signal_connect (G_OBJECT (subitem), "toggle",
                    G_CALLBACK (cb_itemsignal), "toggle");
    g_signal_connect (G_OBJECT (subitem), "expand",
                    G_CALLBACK (cb_itemsignal), "expand");
    g_signal_connect (G_OBJECT (subitem), "collapse",
                    G_CALLBACK (cb_itemsignal), "collapse");

    gtk_tree_append (GTK_TREE (subtree), subitem);
            
    gtk_widget_show (subitem);
}

void addGroup(GtkWidget *widget, Tran_para *mut)
{

    gtk_widget_show(((Tran_para *)mut)->box);
}

int FindPW(int argc, char *argv[])
{
    GtkWidget *window;
    GtkWidget *table;
    GtkWidget *label, *button;
    GtkWidget *nickname;
    GtkWidget *IDname;

    gtk_init(&argc, &argv);
    window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "查找联系人");
    gtk_signal_connect(GTK_OBJECT(window), "destroy", 
            G_CALLBACK(gtk_main_quit), NULL);
    table=gtk_table_new( 3, 4, FALSE);

    label=gtk_label_new("工号");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, 
            GTK_SHRINK, GTK_SHRINK, 10, 10);
    gtk_widget_show(label);
    
    label=gtk_label_new("昵称");
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10, 10);
    gtk_widget_show(label);
    
    IDname=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), IDname, 1, 2, 0, 1, 
            GTK_SHRINK, GTK_SHRINK, 10, 10);
    gtk_widget_show(IDname);

    nickname=gtk_entry_new();
    gtk_table_attach(GTK_TABLE(table), nickname, 1, 2, 1, 2, 
            GTK_SHRINK, GTK_SHRINK, 10, 10);
    gtk_widget_show(nickname);
    
    button=gtk_button_new_with_label("查找");
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
            G_CALLBACK(disFindRes), NULL);
    gtk_table_attach(GTK_TABLE(table), button, 0, 2, 2, 3,
            GTK_SHRINK, GTK_SHRINK, 10, 10);
    gtk_widget_show(button);

    gtk_container_add(GTK_CONTAINER(window), table);
    gtk_widget_show(table); 
    gtk_widget_show(window);
    gtk_main();
}

int disMesg(const struct msg *msgbuf)
{
/*    struct proc proc;

    memset(&proc, 0, sizeof(proc));
    proc.flg = GET_PERSON_INFO;
    proc.argc = 1;
    

    if(writeShm(&proc) == -1){
        printf("Error: disMsg: writeShm\n");
        return -1;
    }

    if(readShm(&proc) == -1){
        printf("Error: disMsg: readShm\n");
        return -1;
    }*/

    GtkTextIter start,end;
    gtk_text_buffer_get_bounds(
            GTK_TEXT_BUFFER(Rcv_buffer), &start, &end);
    gtk_text_buffer_insert(
            GTK_TEXT_BUFFER(Rcv_buffer), &end, "他说:\n", 8);
    gtk_text_buffer_insert(
            GTK_TEXT_BUFFER(Rcv_buffer), &end,
            msgbuf->msg, strlen(msgbuf->msg));
    gtk_text_buffer_insert(
            GTK_TEXT_BUFFER(Rcv_buffer), &end, "\n", 1);
}

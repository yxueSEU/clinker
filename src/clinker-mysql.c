#include "clinker-mysql.h"
#include "check.h"
#include "info.h"
#include <mysql/mysql.h>

int AddBaseInfo(int argc, char argv[][41], struct person *person)
{
    MYSQL *CLinker;
//    MYSQL_RES *res;
    MYSQL_FIELD *field;
    MYSQL_ROW row;
    char querysql[320];
    
    if((CLinker = mysql_init(NULL)) == NULL){
        printf("could not connect\n");
        return -1;
    }
    
    if(mysql_real_connect(CLinker, "localhost", "root", "neusoft",
                "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr,"Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return -1;
    }

    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql, "INSERT INTO BaseInformation "
            "VALUES(%d,'%s', '%s', '%s', '%s', '%s','%s', '%s', %d, '%s')",
            person->ID, argv[0], person->img, person->nickname,
            person->job, person->department, argv[1],
            argv[2], person->state, argv[3]);

    if(mysql_query(CLinker, querysql)){
        printf("AddBaseInfo: query error\n");
        return -1;
    }

/*
    res = mysql_store_result(CLinker);

    if(res)
    {
        printf("Inserted %lu rows\n",
                (unsigned long)mysql_affected_rows(CLinker));
    }
    else
    {
        fprintf(stderr, "Insert error %d: %s\n",
                mysql_errno(CLinker), mysql_error(CLinker));
        return -1;
    }

    mysql_free_result(res);
*/

    mysql_close(CLinker);
   
    return 0;
}

int AddChatlogs(struct msg* msg)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_FIELD *field;
    MYSQL_ROW row;
    char querysql[160];

    if((CLinker = mysql_init(NULL)) == NULL){
        printf("could not connect\n");
        return -1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root", "neusoft",
                "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return -1;
    }

    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql, "INSERT INTO ChatLogs(ID, FriendID, ChatLogs) "
            "VALUES(%d, %d, '%s')", msg->sourceID, msg->targetID, msg->msg);
    if(mysql_query(CLinker, querysql)){
        printf("AddChatlogs: query error\n");
        return -1;
    }
    res = mysql_store_result(CLinker);

    if(res)
    {
        printf("Inserted %lu rows\n",
                (unsigned long)mysql_affected_rows(CLinker));
    }
    else
    {
        fprintf(stderr, "Insert error %d: %s\n",
                mysql_errno(CLinker), mysql_error(CLinker));
        return -1;
    }

    mysql_free_result(res);
    mysql_close(CLinker);

    return 0;
}

int hasID(int ID)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];

    if((CLinker = mysql_init(NULL)) == NULL)
    {
        printf("could not connect\n");
        return -1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return -1;
    }

    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT Nickname FROM BaseInformation WHERE ID = %d", ID);

    if(mysql_query(CLinker, querysql))
    {
        printf("hasID: query error\n");
        return -1;
    }
    
    res = mysql_store_result(CLinker);
   
    if((row = mysql_fetch_row(res)) == NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }

    mysql_free_result(res);
    mysql_close(CLinker);
}

char *getAnswer(int ID)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];
    extern char buf[32];

    if((CLinker = mysql_init(NULL)) == NULL)
    {
        printf("could not connect\n");
        return (void*)-1;
    }
 
    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return (void*)-1;
    }
    
    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT PasswdQuesAn FROM BaseInformation WHERE ID = %d", ID);

    if(mysql_query(CLinker, querysql))
    {
        printf("getAnswer: query error\n");
        return (void *)-1;
    }

    res = mysql_store_result(CLinker);

    if((row = mysql_fetch_row(res)) == NULL)
    {
        printf("error!");
        return (void*)-1;
    }
    else
    {
        memset(buf, 0, sizeof(buf));
        strcpy(buf, row[0]);
        return buf;
    }

    mysql_free_result(res);
    mysql_close(CLinker);
}

char *getpasswd(int ID)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];
    extern unsigned char SHA1PASSWD[41];

    if((CLinker = mysql_init(NULL)) == NULL)
    {
        printf("could not connect\n");
        return (void*)-1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return (void*)-1;
    }
    
    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT Passwd FROM BaseInformation WHERE ID = %d", ID);
    
    if(mysql_query(CLinker, querysql))
    {
        printf("getpasswd: query error\n");
        return (void *)-1;
    }
    
    res = mysql_store_result(CLinker);

    if((row = mysql_fetch_row(res)) == NULL)
    {
        printf("error!");
        return (void*)-1;
    }
    else
    {
        strcpy(SHA1PASSWD, row[0]);
        return SHA1PASSWD;
    }

    mysql_free_result(res);
    mysql_close(CLinker);
}

char *getIP(int ID) 
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];
    extern char buf[32];

    if((CLinker = mysql_init(NULL))==NULL)
    {   
        printf("could not connect\n");
        return (void*)-1;
    }   
 
    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {   
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return (void*)-1;
    }   
    
    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT IP FROM BaseInformation WHERE ID = %d", ID);

    if(mysql_query(CLinker, querysql))
    {
        printf("getIP: query error\n");
        return (void *)-1;
    }

    res = mysql_store_result(CLinker);

    if((row = mysql_fetch_row(res)) == NULL)
    {
        fprintf(stderr, "Error: no IP\n");
        return (void*)-1;
    }
    else
    {
        memset(buf, 0, sizeof(buf));
        strcpy(buf, row[0]);
        return buf;
    }
    mysql_free_result(res);
    mysql_close(CLinker);
}

int Change(int argc, char argv[][6])
{
    MYSQL *CLinker;
    MYSQL_FIELD *field;
    MYSQL_ROW row;
    char querysql[160];
    
    if((CLinker = mysql_init(NULL)) == NULL){
          printf("could not connect\n");
          return -1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root", "neusoft",
                "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr,"Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return -1;
    }

    memset(querysql, 0, sizeof(querysql));
      
    if(strcmp("State", argv[1]) == 0){
        sprintf(querysql, "UPDATE BaseInformation"
                          " SET %s = %d"
                          " WHERE ID = %d",
                          argv[1], atoi(argv[2]), atoi(argv[0]));
    }
    else{
        sprintf(querysql, "UPDATE BaseInformation"
                          " SET %s = '%s'"
                          " WHERE ID = %d",
                          argv[1], argv[2], atoi(argv[0]));
    }

    if(mysql_query(CLinker, querysql)){
          printf("Change: query error\n");
          return -1;
    }

    mysql_close(CLinker);
    return 0;
}

char *getNickname(int ID)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];
    extern char buf[32];

    if((CLinker = mysql_init(NULL)) == NULL)
    {
        printf("could not connect\n");
        return (void*)-1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return (void*)-1;
    }

    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT Nickname FROM BaseInformation WHERE ID = %d", ID);

    if(mysql_query(CLinker, querysql))
    {
        printf("getNickname: query error\n");
        return (void *)-1;
    }

    res = mysql_store_result(CLinker);

    if((row = mysql_fetch_row(res)) == NULL)
    {
        fprintf(stderr, "Error: no nickname\n");
        return (void*)-1;
    }
    else
    {
        memset(buf, 0, sizeof(buf));
        strcpy(buf, row[0]);
        return buf;
    }

    mysql_free_result(res);
    mysql_close(CLinker);
}

int getState(int ID)
{
    MYSQL *CLinker;
    MYSQL_RES *res;
    MYSQL_ROW row;
    char querysql[160];

    if((CLinker = mysql_init(NULL)) == NULL)
    {
        printf("could not connect\n");
        return -1;
    }

    if(mysql_real_connect(CLinker, "localhost", "root",
                "neusoft", "CLinker", 0, NULL, 0) == NULL)
    {
        fprintf(stderr, "Couldn't connect to engine!\n%s\n\n",
                mysql_error(CLinker));
        return -1;
    }

    memset(querysql, 0, sizeof(querysql));
    sprintf(querysql,
            "SELECT State FROM BaseInformation WHERE ID = %d", ID);

    if(mysql_query(CLinker, querysql))
    {
        printf("getState: query error\n");
        return -1;
    }

    res = mysql_store_result(CLinker);

    if((row = mysql_fetch_row(res)) == NULL)
    {
        fprintf(stderr, "Error: no State\n");
        return -1;
    }
    else
    {
        return atoi(row[0]);
    }

    mysql_free_result(res);
    mysql_close(CLinker);
}

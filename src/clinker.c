#include "info.h"
#include "parser.h"

int main()
{
    pid_t pid, pid1;

    if(parser_init() == -1){
        exit(100);
    }

    if(sem_init() == -1){
        exit(99);
    }
    if(parser_set_integer("semid", semid) == -1){
        exit(98);
    }

    if(shm_init() == -1){
        exit(97);
    }
    if(parser_set_integer("shmid", shmid) == -1){
        exit(96);
    }

    printf("clinker.c: shmid: %d semid: %d\n", shmid, semid);

    parser_free();

    if((pid = fork()) == -1){
        perror("clinker.c: fork: pid");
        exit(95);
    }
    if(pid == 0){
        if(execl("./chatcd", "chatcd", (char*)0) == -1){
            perror("clinker.c: execl: chatcd");
            exit(94);
        }
    }
    else{
        if((pid1 = fork()) == -1){
            perror("clinker.c: fork: pid1");
            exit(93);
        }
        if(pid1 == 0){
            if(execl("./nchatcd", "nchatcd", (char*)0) == -1){
                perror("clinker.c: execl: nchatcd");
                exit(92);
            }
        }
        else{
            if(execl("./clinker-gtk", "clinker-gtk", (char*)0) == -1){
                perror("clinker: execl: clinker-gtk");
                exit(91);
            }
        }
    }

    if(shm_free() == -1){
        exit(90);
    }

    if(sem_destroy() == -1){
        exit(89);
    }

    return 0;
}

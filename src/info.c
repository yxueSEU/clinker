#include "global.h"
#include "parser.h"
#include "info.h"
#include "clinker-mysql.h"

int disMsg(const struct msg *msg)
{
    printf("%d %d %s\n", msg->sourceID, msg->targetID, msg->msg);

    return 0;
}

int sem_ops()
{
    /* first one in semaphore set */
    P0.sem_num = 0;
    P0.sem_op = -1;
    P0.sem_flg = 0;

    V0.sem_num = 0;
    V0.sem_op = 1;
    V0.sem_flg = 0;

    /* sencond one in semaphore set */
    P1.sem_num = 1;
    P1.sem_op = -1;
    P1.sem_flg = 0;

    V1.sem_num = 1;
    V1.sem_op = 1;
    V1.sem_flg = 0;

    P2.sem_num = 2;
    P2.sem_op = -1;
    P2.sem_flg = 0;

    V2.sem_num = 2;
    V2.sem_op = 1;
    V2.sem_flg = 0;

    P3.sem_num = 3;
    P3.sem_op = -1;
    P3.sem_flg = 0;

    V3.sem_num = 3;
    V3.sem_op = 1;
    V3.sem_flg = 0;

    return 0;
}

int sem_init()
{
    extern struct sembuf P0, V0, P1, V1, P2, V2, P3, V3;
    extern union semun arg;
    extern int semid;

    sem_ops();

    arg.val = 0;
    unsigned short array[4];
    memset(array, 0, sizeof(array));
    arg.array = array;

    if((semid = semget(IPC_PRIVATE, 4, IPC_CREAT | 0600)) == -1){
        perror("info.c: semget");
        return -1;
    }

    if(semctl(semid, 0, SETALL, arg) == -1){
        perror("info.c: semctl: SETALL");
        return -1;
    }
    return 0;
}

int sem_destroy()
{
    extern int semid;

    if(semctl(semid, 0, IPC_RMID) == -1){
        perror("info.c: semctl_IPC_RMID");
        return -1;
    }
    return -1;
}

int sendMsg(const struct msg *msg)
{
    struct msg *msgbuf;
    if(parser_init() == -1)
    {
        return -1;
    }
    
    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    if((msgbuf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("info.c: sendMsg: shmat");
        return -1;
    }
    memcpy(msgbuf, msg, sizeof(msg));
    if(semop(semid, &V0, 1) == -1){
        perror("info.c: sendMsg: semop");
        return -1;
    }
    if(shmdt(msgbuf) == -1){
        perror("info.c: sendMsg: shmdt");
        return -1;
    }

    parser_free();

    return 0;
}

int shm_init()
{
    extern int shmid;
    if((shmid = shmget(IPC_PRIVATE, BUFSIZ * 2, IPC_CREAT | 0600)) == -1){
        perror("info.c: shmget");
        return -1;
    }
    return 0;
}

int shm_free()
{
    extern int shmid;
    if(shmctl(shmid, IPC_RMID, NULL) == -1){
        perror("info.c: shmctl_IPC_RMID");
        return -1;
    }
    return 0;
}

char *gethomedir()
{
    char *homedir = getpwuid(getuid())->pw_dir;
    if(!homedir){
        perror("info.c: getpwuid");
        return (void*)-1;
    }
    return homedir;
}

int writeShm(const struct proc *proc)
{
    struct proc *procbuf;
    extern int semid, semid;

    if(parser_init() == -1){
        return -1;
    }

    semid = atoi(parser_get_value("semid"));
    shmid = atoi(parser_get_value("shmid"));
    printf("semid: %d shmid: %d\n", semid, shmid);

    if(proc == NULL){
        return 0;
    }

    if((procbuf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("info.c: writeShm: shmat");
        return -1;
    }

    memcpy(procbuf, proc, sizeof(*procbuf));

    if(shmdt(procbuf) == -1){
        perror("info.c: writeShm: shmdt");
        return -1;
    }
    if(semop(semid, &V3, 1) == -1){
        perror("info.c: semop: V3");
        return -1;
    }
    
    parser_free();

    return 0;
}

int readShm(struct proc *proc)
{
    struct proc *procbuf;
    extern int semid, shmid;

    if(parser_init() == -1){
        return -1;
    }

    semid = atoi(parser_get_value("semid"));
    shmid = atoi(parser_get_value("shmid"));

    if(semop(semid, &P2, 1) == -1){
        perror("info.c: readShm: semop");
        return -1;
    }
    if((procbuf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("info.c: readShm: shmat");
        return -1;
    }
    memcpy(proc, procbuf, sizeof(*proc));
    if(shmdt(procbuf) == -1){
        perror("info.c: readShm: shmdt");
        return -1;
    }

    parser_free();

    return 0;
}

int sendFile(const struct proc* proc)
{
    struct proc* procbuf;
    char localIP[16];

    if(parser_init() == -1)
    {
        return -1;
    }

    semid = atoi(parser_get_value("semid"));
    shmid = atoi(parser_get_value("shmid"));
    strcpy(localIP, parser_get_value("localIP"));

    parser_free();

    if((procbuf = shmat(shmid, NULL, 0)) == (void *)-1)
    {
        perror("sendfile.c:shmat");
        exit(40);
    }

    memcpy(procbuf, proc, sizeof(*procbuf));

    if(semop(semid, &V3, 1) == -1)
    {
        perror("sendfile.c:semop:V3");
        exit(39);
    }
    
    if(shmdt(procbuf) == -1){
        perror("info.c: sendFile: shmdt");
        return -1;
    }

    int sockfd, pin_len, new_sockfd;
    struct sockaddr_in addr, pin_addr;

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(localIP);
    addr.sin_port = htons(INFO_PORT);

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(41);
    }
    if(bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
    {
        perror("bind");
        exit(42);
    }
    if(listen(sockfd, 5) < 0)
    {
        perror("listen");
        exit(43);
    }
    if((new_sockfd = accept(sockfd,
                    (struct sockaddr*)&pin_addr, &pin_len)) < 0)
    {
        perror("accept");
        exit(44);
    }

    char buf[BUFSIZ];
    int fd;
    fd = open(proc->argv[2], O_RDONLY);
    if(fd == -1)
    {
    	perror("open");
    	exit(45);
    }

    int nread;
    do{
        memset(buf, 0, BUFSIZ);
        nread=read(fd, buf, BUFSIZ);
        if(nread == -1)
        {
            perror("read");
            exit(46);
        }

        size_t send_t;
        send_t = send(new_sockfd, buf, nread, 0);
        if(send_t==-1)
        {
            perror("sendto");
            exit(48);
        }
    }while(nread == BUFSIZ);

    return 0;
}

int revcfile(struct proc* proc)
{
    struct proc *procbuf, *proctmp;
    
    parser_init();

    semid = atoi(parser_get_value("semid"));
    shmid = atoi(parser_get_value("shmid"));
    
    if((procbuf = shmat(shmid, NULL, 0)) < 0)
    {
        perror("shmat");
        exit(50);
    }

    if(semop(semid, &P2, 1) == -1)
    {
        perror("semop_P2");
        exit(51);
    }

    memcpy(proctmp, procbuf, sizeof(proctmp));

    char filename[255];
    memset(filename, 0, sizeof(filename));

    sprintf(filename, "%s/%s", proctmp->argv[0], basename(proc->argv[2]));

    parser_free();

    int fd;
    fd = open(filename, O_CREAT, 0600);

    if(fd == -1)
    {
        perror("creat");
        exit(49);
    }

    int sockfd,sin_len, nwrite;
    struct sockaddr_in sin_addr;
    char buf[BUFSIZ];

 
    sin_addr.sin_family=AF_INET;
    sin_addr.sin_addr.s_addr=inet_addr(procbuf->argv[1]);
    sin_addr.sin_port=htons(INFO_PORT);
    sin_len=sizeof(sin_addr);
    if((sockfd=socket(AF_INET, SOCK_STREAM, 0))==-1)
    {
	perror("socket");
	exit(52);
    }
    do{
        memset(buf, 0, BUFSIZ);
        if(recv(sockfd, buf, BUFSIZ, 0) == -1)
        {
            perror("recv");
            exit(53);
        }
        nwrite =  write(fd, buf, BUFSIZ);
    }while(nwrite == BUFSIZ);

    return 0;
}

#include "global.h"
#include "info.h"
#include "parser.h"
    
int main()
{
    int sockfd, flag;
    pid_t pid;
    struct sockaddr_in addr, saddr;
    char localIP[16], serverIP[16];
    struct proc *procbuf;
    socklen_t saddr_len;
    size_t proc_len;
    
    memset(localIP, 0, sizeof(localIP));
    memset(serverIP, 0, sizeof(serverIP));

    saddr_len = sizeof(saddr);
    proc_len = sizeof(*procbuf);
    parser_init();

    strcpy(localIP, parser_get_value("localIP"));
    strcpy(serverIP, parser_get_value("serverIP"));
    semid = atoi(parser_get_value("semid"));
    shmid = atoi(parser_get_value("shmid"));

    if((procbuf = shmat(shmid, NULL, 0)) == (void *)-1)
    {
        perror("nchatcd.c: shmat");
        exit(49);
    }
    parser_free();
    
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(localIP);
    addr.sin_port = htons(INFO_PORT);
	
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = inet_addr(serverIP);
    saddr.sin_port = htons(INFO_PORT);
	
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("nchatcd.c: socker");
        exit(13);
    }
    if((pid = fork()) == -1)
    {
        perror("nchatcd.c: fork");
        exit(14);
    }
    if(pid == 0)
    {
        if(bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        {
            perror("nchatcd.c: bind");
            exit(15);
        }
        while(1)
        {
            if(recvfrom(sockfd, procbuf, sizeof(*procbuf),
                        0, (struct sockaddr*)&saddr, &saddr_len) == -1)
            {	
                perror("nchatcd.c: recvfrom");
                exit(16);
            }
            printf("recvfrom: argv[0]: %s\n", procbuf->argv[0]);
            if(semop(semid, &V2, 1) == -1)
            {
                perror("nchatcd.c: semop: V2");
                exit(38);
            }
        }
    }
    else
    {
        while(1)
        {
            if(semop(semid , &P3, 1) == -1)
            {
                perror("nchatcd.c: semop_P3");
                exit(17);
            }
            printf("sendto: argv[0]: %s\n\n", procbuf->argv[0]);
            if(sendto(sockfd, procbuf, proc_len, 0,
                        (struct sockaddr*)&saddr, saddr_len) == -1)
            {
                perror("nchatcd.c: sendto");
            }
        }
    }
    if(close(sockfd) == -1)
    {
	perror("nchatcd.c: close");
	exit(18);
    }
    if(shmdt(procbuf) == -1)
    {
	perror("nchatcd.c: shmdt");
	exit(19);
    }
    return 0;
}

#include "info.h"
#include "check.h"
#include "parser.h"

int main()
{
    struct tmp *buf, resbuf;

    if(parser_init() == -1){
        exit(31);
    }

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    parser_free();

    if((buf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("nchatpd.c: shmat: procbuf");
        exit(32);
    }

    while(1){
        if(semop(semid, &P2, 1) == -1){
            perror("nchatpd.c: semop: P2");
            exit(33);
        }

        if(checkInfo(buf, &resbuf) == -1){
            fprintf(stderr, "Warning: checkInfo\n");
        }

        memcpy(buf, &resbuf, sizeof(*buf));

        printf("nchatpd: just before desuspend: "
                "%s %d %d %s %s\n\n",
                inet_ntoa(buf->addr.sin_addr),
                ntohs(buf->addr.sin_port),
                buf->proc.flg, buf->proc.argv[0],
                buf->proc.argv[1]);

        if(semop(semid, &V3, 1) == -1){
            perror("nchatpd.c: semop: V3");
            exit(37);
        }
    }

    if(shmdt(buf) == -1){
        perror("nchatpd.c: shmdt");
        exit(67);
    }
    return 0;
}

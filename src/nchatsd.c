#include "info.h"
#include "parser.h"

int main()
{
    int sockfd;
    char localIP[16];
    struct tmp *buf;
    struct sockaddr_in addr;
    socklen_t taddr_len;
    pid_t pid;
    size_t proc_len;

    if(parser_init() == -1){
        exit(18);
    }
    strcpy(localIP, parser_get_value("localIP"));

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

/*    if(shm_init() == -1){
        exit(19);
    }
    if(parser_set_integer("shmid", shmid) == -1){
        exit(20);
    }
    if(sem_init() == -1){
        exit(21);
    }
    if(parser_set_integer("semid", semid) == -1){
        exit(22);
    } */

    parser_free();

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(localIP);
    addr.sin_port = htons(INFO_PORT);

    if((buf = shmat(shmid, NULL, 0)) == (void*)-1){
        perror("nchatsd.c: shmat");
        exit(23);
    }

    proc_len = sizeof(buf->proc);
    buf->addr_len = sizeof(buf->addr);

    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
        perror("nchatsd.c: socket");
        exit(26);
    }

    if((pid = fork()) == -1){
        perror("nchatsd.c: fork");
        exit(27);
    }

    if(pid == 0){
        if(bind(sockfd, (struct sockaddr*)&addr, sizeof(addr)) == -1){
            perror("nchatsd.c: bind");
            exit(28);
        }
        while(1){
            if(recvfrom(sockfd, &buf->proc, proc_len, 0,
                    (struct sockaddr*)&buf->addr,
                    &buf->addr_len) == -1){
                perror("nchatsd.c: recvfrom");
            }
            printf("\nnchatsd.c: recvfrom: %s %d %d %s %s\n\n",
                    inet_ntoa(buf->addr.sin_addr),
                    ntohs(buf->addr.sin_port),
                    buf->proc.flg, buf->proc.argv[0],
                    buf->proc.argv[1]);
            if(semop(semid, &V2, 1) == -1){
                perror("nchatsd.c: semop");
                exit(29);
            }
        }
    }
    else{
        while(1){
            if(semop(semid, &P3, 1) == -1){
                perror("nchatsd.c: semop");
                exit(30);
            }
            if(sendto(sockfd, &buf->proc, proc_len, 0,
                        (struct sockaddr*)&buf->addr, buf->addr_len) == -1){
                perror("nchatsd.c: sendto");
            }
        }
    }

    if(shmdt(buf) == -1){
        perror("nchatsd.c: shmdt");
        exit(66);
    }

    if(close(sockfd) == -1){
        perror("nchatsd.c: close");
        exit(65);
    }

    return 0;
}

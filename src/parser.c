#include "parser.h"
#include "info.h"
#include <string.h>

char *get_gkfile_path()
{
    size_t len = strlen(gethomedir()) + 9;
    gkfile_path = (char*)malloc(len + 1);

    if(sprintf(gkfile_path, "%s/%s", gethomedir(), ".clinker") != len){
        perror("parser.c: sprintf");
        return (void*)-1;
    }
    return gkfile_path;
}

int parser_init()
{
    extern GKeyFile *gkfile;
    extern char *gkfile_path;
    GError *error = NULL;

    sem_ops();

    gkfile_path = get_gkfile_path();
    gkfile = g_key_file_new();
    if(!g_key_file_load_from_file(
                gkfile, gkfile_path, G_KEY_FILE_NONE, &error)){
        if(error){
            fprintf(stderr, "parser_init: %s\n", error->message);
            g_error_free(error);
            return -1;
        }
    }
    return 0;
}

void parser_free()
{
    extern GKeyFile *gkfile;
    extern char *gkfile_path;
    extern char *gkfile_path;

    g_key_file_free(gkfile);
    free(gkfile_path);
    g_free(gkfile_value);
}

char *parser_get_value(const char *key)
{
    extern GKeyFile *gkfile;
    extern char *gkfile_value;
    GError *error = NULL;

    gkfile_value = g_key_file_get_value(gkfile, "default", key, &error);
    if(error){
        fprintf(stderr, "parser_get_value: %s", error->message);
        g_error_free(error);
        return (void*)-1;
    }
    return gkfile_value;
}

int parser_set_integer(const char *key, int value)
{
    extern GKeyFile *gkfile;
    gchar *gkfile_content;
    gsize len = 0;
    GError *error = NULL;
    int fd;

    g_key_file_set_integer(gkfile, "default", key, value);
    gkfile_content = g_key_file_to_data(gkfile, &len, &error);
    if(error){
        fprintf(stderr, "parser_set_integer: %s\n", error->message);
        g_error_free(error);
        return -1;
    }

    if((fd = open(gkfile_path, O_RDWR)) == -1){
        perror("parser.c: open");
        return -1;
    }
    if(write(fd, gkfile_content, len) != len){
        perror("parser.c: write");
        return -1;
    }
    g_free(gkfile_content);
    if(close(fd) == -1){
        perror("parser.c: close");
        return -1;
    }
    return 0;
}

int parser_set_value(const char *key, const char *value)
{
    extern GKeyFile *gkfile;
    gchar *gkfile_content;
    gsize len = 0;
    GError *error = NULL;
    int fd;

    g_key_file_set_value(gkfile, "default", key, value);
    gkfile_content = g_key_file_to_data(gkfile, &len, &error);
    if(error){
        fprintf(stderr, "parser_set_value: %s\n", error->message);
        g_error_free(error);
        return -1;
    }

    if((fd = open(gkfile_path, O_RDWR)) == -1){
        perror("parser.c: open");
        return -1;
    }
    if(write(fd, gkfile_content, len) != len){
        perror("parser.c: write");
        return -1;
    }
    g_free(gkfile_content);
    if(close(fd) == -1){
        perror("parser.c: close");
        return -1;
    }
    return 0;
}

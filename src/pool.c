#include "info.h"
#include "parser.h"

int main()
{
    pid_t pid, pid1;

    if(parser_init() == -1){
        exit(88);
    }

    if(shm_init() == -1){
        exit(87);
    }
    if(parser_set_integer("shmid", shmid) == -1){
        exit(86);
    }
    if(sem_init() == -1){
        exit(85);
    }
    if(parser_set_integer("semid", semid) == -1){
        exit(84);
    }

    parser_free();

    if((pid = fork()) == -1){
        perror("pool.c: fork: pid");
        exit(83);
    }
    if(pid == 0){
        if(execl("./nchatsd", "nchatsd", (char*)0)){
            perror("pool.c: execl: nchatsd");
            exit(82);
        }
    }
    else{
        if((pid1 = fork()) == -1){
            perror("pool.c: fork: pid1");
            exit(81);
        }
        if(pid1 == 0){
            if(execl("./nchatpd", "nchatpd", (char*)0) == -1){
                perror("pool.c: execl: nchatpd");
                exit(80);
            }
        }
        else{
            if(execl("./chatsd", "chatsd", (char*)0) == -1){
                perror("pool.c: execl: chatsd");
                exit(79);
            }
        }
    }

    if(shm_free() == -1){
        exit(78);
    }

    if(sem_destroy() == -1){
        exit(77);
    }

    return 0;
}

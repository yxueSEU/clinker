#include "info.h"
#include "parser.h"

int main()
{
    parser_init();

    shmid = atoi(parser_get_value("shmid"));
    semid = atoi(parser_get_value("semid"));

    shmctl(shmid, IPC_RMID, NULL);
    perror("shmctl");

    semctl(semid, 0, IPC_RMID);
    perror("semctl");

    parser_free();

    return 0;
}

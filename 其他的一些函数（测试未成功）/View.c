#if define(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif
#include "project.h"
#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>

int View(int argc,char **argv)
{
    MYSQL mysql,*sock;
    MYSQL_RES *res;
    MYSQL_FIELD *fd;
    MYSQL_ROW row;
    char qbuf[160];

    if(argc != 2 )
    {
        fprintf(stderr,"usage:mysql_select <userid>nn");
        exit(1);
    }

    if (!(sock = mysql_real_connect(&CLinker,"localhost","root","neusoft",0,NULL,0)))
    {
        frintf(stderr,"Could connect to engine!n%snn",mysql_error(&mysql));
        perror("");
        exit(1);
    }

    sprintf(qbuf,SELECT_QUERY,atoi(argv[1]));

    if(mysql_query(sock,qbuf))
    {
        fprintf(stderr,"Query failed (%s)n",mysql_error(sock));
        exit(1);
    }
  
    if(!(res=mysql_store_result(sock)))
    {
        fprintf(stderr,"Could get result from %sn",mysql_error(sock));
        exit(1);
    }

    printf("number of fields returned: %dn",mysql_num_fields(res));

    while (row = mysql_fecth_row(res))
    {
        printf("Their userid #%d s username is: %sn",atoi(argv[1]),
                (((row[0]==NULL)&&(!strlen(row[0]))) ? "NULL":row[0]));
        puts("query ok !n");
    }

    mysql_free_result(res);
    mysql_close(sock);
    exit(0);
    return 0;
}

